<!-- Форма для логирование в учетную запись -->
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Sign In</h3>
				<div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div>
			</div>


			<div class="card-body">
				<!-- Обычная форма регистрации -->
				<form action="login.php" method="POST">

					<div class="input-group form-group">
						<input type="text" name="login" class="form-control" placeholder="login" required>
					</div>

					<div class="input-group form-group">
						<input type="password" name="password" class="form-control" placeholder="password" required>
					</div>

					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
				<!-- Кнопка подключения логирование через телеграм -->
				<div class="telegramloginbotton">
					<?php 
						require_once "../../cgi/Accaunt/login_teleg/telega_login.php";
				 	?>
				</div>
			</div>


			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					<!-- Ссылка на регестрацию -->
					Нет аккаунта?<a href="../../cgi/Accaunt/signup.php">Зарегестрироваться</a>
				</div>
				<div class="d-flex justify-content-center">
					<!-- Забыли пароль -->
					<a href="#">Забыли пароль?</a>
				</div>
			</div>
		</div>
	</div>