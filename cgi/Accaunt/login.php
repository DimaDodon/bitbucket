<!DOCTYPE html>
<html lang="en">
<head>
  <?php require_once '../../design/template/meta.html';?>
  <link rel="stylesheet" type="text/css" href="../../styles/login.css">
  <title>Login</title>
</head>
<body>
  <?php require_once '../../design/template/header.html';?>

	<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_COOKIE['user'])) {
	//если запрос методом гет и если куки уже установлены то выводим приветсвие
	$data = unserialize($_COOKIE['user']);

	//проверка на не потдельные ли куки
	//подключаем базу данных
	require_once '../DataBaseConnection/DataBaseConnection.php';

	try {
		$db = new DataBase(); //создаем подключение к базе данных
	} catch (Exception $e) {
		echo "$e";
	}

	//берем из переменной $data логин

	try {
		$isUser = $db->query('SELECT * FROM `users` WHERE `login` = \'' . $data[0]['login'] . '\'');
	} catch (Exception $e) {
		echo "$e";
	}

	if ($isUser) {
		$hash = $isUser[0]['password'];
		//проверка на длину хэша
		if (strlen($hash) == 60) {

			echo "Hello, " . $data[0]['login'] . ".<br>";

			echo "<a href=\"logout.php\">Выйти</a>";
		}
	} else {
		echo "Простите, но вы потделали куки файлы";
	}

} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//если пользователь не вошел, но отправил вход
	// Если были переданы данные

	require_once '../DataBaseConnection/DataBaseConnection.php'; //подключаем файл для поключения к базе данных

	// Пробуем подключиться к базе данных
	try {
		$db = new DataBase(); //Создание подключение к базе данных
	} catch (Exception $e) {
		echo "Couldn't create user: " . $e->getMessage();
	}

	//выполняем все проверки для входа
	//подготовка данных для взятие записи
	$login = trim(htmlspecialchars($_POST['login']));
	$password = trim(htmlspecialchars($_POST['password']));

	if ($login && $password) {
		//проверка на то что не передали много пробелов вместо записи
		//Создание запроса к базе данных с взятием пользователя

		$res = $db->query('SELECT `login`, `password`, `email`
		 		FROM `users`
		 		WHERE `login` = \'' . $login . '\'');

		if (isset($res[0]['login'])) {
			$hashPass = password_verify($password, $res[0]['password']);

			if ($hashPass) {
				//нашли пользователя с такими данными тогда логинемся
				$res = serialize($res); //серилизуем данные для хранения в куках
				setcookie('user', $res, time() + 86400, '/'); //устанавливаем куки с жизнью в сутки
				header('Location: login.php');
			} else {
				//не нашли пользователя с такми данными
				echo "<br>Не правильный пароль!";
			}

		} else {
			echo "Такого пользователя не найдено!";
		}

	} else {
		//если передали много пробелов вместо строки
		echo "Заполните все поля";
	}

} else {
	require_once '../../design/template/login-form.php';
}

?>


<?php require_once '../../design/template/footer.html';?>
</body>
</html>




