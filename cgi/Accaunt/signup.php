<!DOCTYPE html>
<html lang="en">
<head>
	<?php	require_once '../../design/template/meta.html';?>
	<link rel="stylesheet" type="text/css" href="../../styles/signup.css">
	<title>Sign up</title>
</head>
<body>
<?php require_once '../../design/template/header.html';?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	//если была запрошена просто форма для регистрации
	require_once '../../design/template/signup.html';
} else {
	//если была передана форма для регистрации
	//берем все данные переданные нам из формы
	$login = trim(htmlspecialchars($_POST['login']));
	$password0 = trim(htmlspecialchars($_POST['password0']));
	$password1 = trim(htmlspecialchars($_POST['password1']));
	$email = trim(htmlspecialchars($_POST['email']));
	//проверяем на пустоту
	if ($login && $password0 && $password1 && $email) {
		//если все поля введены
		//теперь проверяем на то совпадают ли пароли
		if ($password0 == $password1) {
			//если пароли совпадают
			//если проверки все пройдены то подключаемся к базе данных
			require_once '../DataBaseConnection/DataBaseConnection.php';

			try {
				$db = new DataBase(); //создаем новое подключение к базе данных
			} catch (Exception $e) {
				echo "Невозможно создать подключение к базе даных. $e";
			}

			//проверяем существует ли таблица пользователей в базе данных, если такой нет, то мы её создаем
			if (!($db->execute('SELECT * FROM `users`'))) {
				$db->execute('
			    CREATE TABLE USERS(
			      id INT NOT NULL  AUTO_INCREMENT,
			      login VARCHAR (50) NOT NULL,
			      password VARCHAR (60) NOT NULL,
			      email VARCHAR(100),
			      PRIMARY KEY (id)
			    );');
			}
			//берем данные из базы данных для проверки уникальности создоваемой записи
			$dblogin = $db->query('SELECT `login` FROM `users` WHERE `login` = \'' . $login . '\'');
			$dbemail = $db->query('SELECT `email` FROM `users` WHERE `email` = \'' . $email . '\'');

			if ($dblogin) {
				//проверка на то сушествует ли такой логин
				echo "Простите но пользователь с таким логином уже существует.";
				exit();
			} elseif ($dbemail) {
				//проверяем на уникальность почту
				echo "Простите но пользователь с таким электронным адресом уже существует
					";
				exit();
			} else {
				//выполнение кодирования пароля для безопасного хранения в базе данных
				$password0 = password_hash($password0, PASSWORD_BCRYPT);
				//все проверки пройдены успешно, регистрируем
				$db->execute("INSERT INTO `users` SET `login` = '" . $login . "' , `password` = '" . $password0 . "' ,`email` = \"" . $email . "\"");
				//производим логирование
				$res = [0 => ['login' => $login, 'password' => $password0, 'email' => $email]];
				$res = serialize($res);
				setcookie('user', $res, time() + 86400, '/');
				//header('Location: login.php');
			}

			//проверяем не существует ли в базе данных пользователя с таким же логином или электронной почтой

		} else {
			//если пароли не совпадают
			echo "Пароли не совпадают";
			exit();
		}
	} else {
		//если каое то поле пустое
		echo "Заполните ве поля";
		exit();

	}
}

?>
<?php require_once '../../design/template/footer.html';?>
</body>
</html>
