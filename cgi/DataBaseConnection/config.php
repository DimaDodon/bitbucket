<?php
return [
	'host' => 'localhost',
	'db_name' => 'new_films',
	'username' => 'homestead',
	'password' => 'secret',
	'charset' => 'utf8',
];